global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global read_char
global read_word
global parse_uint
global parse_int
global string_equals
global string_copy



section .text
 
; аргументы: rdi -- код возврата (0 - успешно)
; завершает текущий процесс
exit: 
    mov rax, 60
    syscall


; аргументы: rdi -- указатель на нуль-терминированную строку
; возвращает: rax -- длина строки
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi + rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret


; аргументы: rdi -- указатель на нуль-терминированную строку, 
; выводит строку в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret


; аргументы: rdi --  код символа
; выводит символ в stdout
print_char:
    push rdi
	mov rsi, rsp
    mov rdx, 1
    mov rax, 1
    mov rdi, 1
    syscall
    pop rdi
    ret


; переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA 
    call print_char
    ret


; аргументы: rdi -- беззнаковое 8-байтовое число 
; выводит число в десятичном формате 
print_uint:
	mov rax, rdi
	mov rdi, rsp
	sub rsp, 32; максимальное 8-байтовое число (18446744073709551615) содержит 20 символов + 1 для нуль-терминатора -- 21 < 32
	dec rdi
	mov byte[rdi], 0
	mov r10, 10
.loop:
	xor rdx, rdx
	div r10
	add rdx, '0'
	dec rdi
	mov [rdi], dl
	test rax, rax
	jne .loop
	
	call print_string
	add rsp, 32
	ret


; аргументы: rdi -- беззнаковое 8-байтовое число 
; выводит число в десятичном формате  
print_int:
    test rdi, rdi
	jge .print
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	.print:
	jmp print_uint


; аргументы: rsi, rdi --  два указателя на нуль-терминированные строки
; возвращает 1 если строки равны, 0 иначе
string_equals:
    .loop:
		mov al, byte[rsi]
		cmp byte[rdi], al
		jne .not_equals
		cmp byte[rdi], 0
		je .equals
		inc rsi
		inc rdi
		jmp .loop
	.not_equals:
		xor rax, rax
		ret
	.equals:
		mov rax, 1
		ret


; читает один символ из stdin
; возвращает: rax -- код символа (0 если достигнут конец потока)
read_char:	
    push 0
	mov rsi, rsp
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    syscall
    pop rax
    ret 


; аргументы: rdi -- адрес начала буфера, rsi -- размер буфера
; читает в буфер слово из stdin, пропуская пробельные символы (0x20, 0x9, 0xA.) в начале, дописывает к слову нуль-терминатор.
; возвращает: rax -- адрес буфера, rdx -- длинa слова (0 и 0, если слово не поместилось в буфер)
read_word:
	push r12
	push r13
	push r14
	mov r12, rdi
	mov r13, rsi
    xor r14, r14
    
    .A:
    	call read_char
		
    	cmp rax, 0x20; пробел
    	je .A
    	cmp rax, 0x9; таб
    	je .A
    	cmp rax, 0xA; перенос стоки
    	je .A
      
    .B:	
		cmp r14, r13
    	je .E	
		
    	test rax, rax; конец ввода
    	je .R
    	cmp rax, 0x20; пробел
    	je .R
    	cmp rax, 0x9; таб
    	je .R
    	cmp rax, 0xA; перенос стоки
    	je .R 
		
    	
    	mov [r12 + r14], rax
    	
    	call read_char

		inc r14
    	jmp .B
		
    .R:
    	mov word[r12 + r14 + 1], 0
    	mov rax, r12
		mov rdx, r14
		jmp .end
		
    .E:
    	xor rax, rax
		
	.end:
		pop r14
		pop r13
		pop r12
    	ret
		
		
; аргументы: rdi -- указатель на строку
; пытается прочитать из её начала беззнаковое число.
; возвращает: rax -- число, rdx -- его длина в символах( включая знак, если он был, 0 -- если число прочитать не удалось)
parse_int:
    cmp byte[rdi], '-'
    jne .parse_uint
	
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 		
		
	.parse_uint:
 

; аргументы: rdi -- указатель на строку
; пытается прочитать из начала беззнаковое число.
; возвращает: rax -- число, rdx -- его длина в символах( 0 -- если число прочитать не удалось)
parse_uint:
    xor rax, rax
    xor rcx, rcx ; текущая длинa
    xor rsi, rsi ; текущий символ
    mov r10, 10
	
    .A:
    	mov sil, byte[rdi + rcx]
    	cmp sil, '0'
    	jl .R
    	cmp sil, '9'
    	jg .R
		
    	mul r10
    	add rax, rsi
		sub rax, '0'
    	inc rcx
		
    	jmp .A
		
    .R:
		mov rdx, rcx
    	ret





; аргументы: rdi -- указатель на строку, rsi -- указатель на буфер, rdx -- длина буфера
; копирует строку в буфер
; возвращает: rax -- длина строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	.loop:
		cmp rax, rdx
		je .overflow
		mov cl, byte[rdi + rax]
		mov byte[rsi + rax], cl
		test cl, cl
		je .copied
		inc rax
	jmp .loop
	
	.overflow:
		xor rax, rax
	.copied:
		ret
